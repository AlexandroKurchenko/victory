package svictory.woopss.com.smallvictory.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import svictory.woopss.com.smallvictory.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}
